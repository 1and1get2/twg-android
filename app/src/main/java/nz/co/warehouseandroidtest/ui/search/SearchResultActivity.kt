package nz.co.warehouseandroidtest.ui.search

import android.os.Bundle
import nz.co.warehouseandroidtest.utils.PreferenceUtil
import android.graphics.Rect
import android.view.View
import nz.co.warehouseandroidtest.Constants
import nz.co.warehouseandroidtest.R
import nz.co.warehouseandroidtest.base.BaseActivity
import nz.co.warehouseandroidtest.data.*
import nz.co.warehouseandroidtest.domain.source.datastore.WarehouseRemoteDataSource
import java.util.ArrayList
import java.util.HashMap
import javax.inject.Inject

class SearchResultActivity : BaseActivity() {
    @Inject
    lateinit var remote: WarehouseRemoteDataSource

    private var mKeyWord: String? = null
    private var recyclerView: androidx.recyclerview.widget.RecyclerView? = null
    private var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout? = null
    private var searchResultAdapter: SearchResultAdapter? = null
    private val data: MutableList<ProductWithoutPrice?> = ArrayList()
    private var totalItemNum: String? = null
    private var startIndex = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        swipeRefreshLayout = findViewById<View>(R.id.refresh_layout) as androidx.swiperefreshlayout.widget.SwipeRefreshLayout
        swipeRefreshLayout!!.setOnRefreshListener {
            data.clear()
            loadData(0, 20)
            startIndex = 0
            swipeRefreshLayout!!.postDelayed({
                if (swipeRefreshLayout != null && swipeRefreshLayout!!.isRefreshing) {
                    swipeRefreshLayout!!.isRefreshing = false
                }
            }, 1000)
        }
        recyclerView = findViewById<View>(R.id.recycler_view) as androidx.recyclerview.widget.RecyclerView
        recyclerView!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recyclerView!!.addItemDecoration(object : androidx.recyclerview.widget.DividerItemDecoration(this, VERTICAL) {
            override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.top = this@SearchResultActivity.resources.getDimensionPixelOffset(R.dimen.recyclerview_out_rec_top)
            }
        })
        searchResultAdapter = SearchResultAdapter()
        searchResultAdapter!!.setData(data)
        recyclerView!!.adapter = searchResultAdapter
        recyclerView!!.addOnScrollListener(object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                searchResultAdapter!!.setLoadState(searchResultAdapter!!.LOADING)
                if (data.size < totalItemNum!!.toInt()) {
                    loadData(startIndex, 20)
                } else {
                    searchResultAdapter!!.setLoadState(searchResultAdapter!!.LOADING_END)
                }
            }
        })
        mKeyWord = intent.extras.getString(FLAG_KEY_WORD)
        loadData(startIndex, 20)
    }

    private fun loadData(startIndex: Int, itemsPerPage: Int) {
        val paramsMap = HashMap<String?, String?>()
        paramsMap["Search"] = mKeyWord
        paramsMap["MachineID"] = Constants.MACHINE_ID
        paramsMap["UserID"] = PreferenceUtil.getUserId(this)
        paramsMap["Branch"] = Constants.BRANCH_ID.toString()
        paramsMap["Start"] = startIndex.toString()
        paramsMap["Limit"] = itemsPerPage.toString()
        /*
        (applicationContext as WarehouseTestApp).warehouseService.getSearchResult(paramsMap).enqueue(object : Callback<SearchResult?> {
            override fun onResponse(call: Call<SearchResult?>, response: Response<SearchResult?>) {
                if (response.isSuccessful) {
                    val searchResult = response.body() as SearchResult?
                    val ifFound = searchResult!!.Found
                    if (ifFound == "Y") {
                        totalItemNum = searchResult.HitCount
                        this@SearchResultActivity.startIndex += 20
                        for (i in searchResult.Results!!.indices) {
                            val item = searchResult.Results!![i]
                            data.add(item!!.Products!![0])
                        }
                        searchResultAdapter!!.setData(data)
                        searchResultAdapter!!.setLoadState(searchResultAdapter!!.LOADING_COMPLETE)
                    }
                } else {
                    Toast.makeText(this@SearchResultActivity, "Search failed!", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<SearchResult?>, t: Throwable) {
                Toast.makeText(this@SearchResultActivity, "Search failed!", Toast.LENGTH_LONG).show()
            }
        })*/
    }

    companion object {
        var FLAG_KEY_WORD = "keyWord"
    }
}
