package nz.co.warehouseandroidtest.ui.search

import android.widget.TextView
import android.widget.ProgressBar
import android.widget.LinearLayout
import android.view.View
import nz.co.warehouseandroidtest.R

class FooterViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    var pbLoading: ProgressBar
    var tvLoading: TextView
    var llEnd: LinearLayout

    init {
        pbLoading = itemView.findViewById<View>(R.id.pb_loading) as ProgressBar
        tvLoading = itemView.findViewById<View>(R.id.tv_loading) as TextView
        llEnd = itemView.findViewById<View>(R.id.ll_end) as LinearLayout
    }
}
