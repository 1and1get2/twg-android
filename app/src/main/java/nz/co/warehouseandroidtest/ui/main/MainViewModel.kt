package nz.co.warehouseandroidtest.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.domain.usecase.GetUserIdUseCase
import javax.inject.Inject


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getUserIdUseCase: GetUserIdUseCase
) : ViewModel(){

    suspend fun loadUser() = getUserIdUseCase.getUserId(true)

}
