package nz.co.warehouseandroidtest.ui.productdetail

import android.os.Bundle
import nz.co.warehouseandroidtest.utils.PreferenceUtil
import android.view.View
import android.widget.*
import nz.co.warehouseandroidtest.Constants
import nz.co.warehouseandroidtest.R
import nz.co.warehouseandroidtest.base.BaseActivity
import nz.co.warehouseandroidtest.domain.source.datastore.WarehouseRemoteDataSource
import java.util.HashMap
import javax.inject.Inject

class ProductDetailActivity : BaseActivity() {
    @Inject
    lateinit var remote: WarehouseRemoteDataSource

    private var ivProduct: ImageView? = null
    private var tvProduct: TextView? = null
    private var ivClearance: ImageView? = null
    private var tvPrice: TextView? = null
    private var tvBarcode: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        ivProduct = findViewById<View>(R.id.iv_product) as ImageView
        tvProduct = findViewById<View>(R.id.tv_product) as TextView
        ivClearance = findViewById<View>(R.id.iv_clearance) as ImageView
        tvPrice = findViewById<View>(R.id.tv_price) as TextView
        tvBarcode = findViewById<View>(R.id.tv_barcode) as TextView
        val barCode = intent.extras.getString(FLAG_BAR_CODE)
        val paramMap = HashMap<String?, String?>()
        paramMap["BarCode"] = barCode
        paramMap["MachineID"] = Constants.MACHINE_ID
        paramMap["UserID"] = PreferenceUtil.getUserId(this)
        paramMap["Branch"] = Constants.BRANCH_ID.toString()

        /*
        (applicationContext as WarehouseTestApp).warehouseService.getProductDetail(paramMap).enqueue(object : Callback<ProductDetail?> {
            override fun onResponse(call: Call<ProductDetail?>, response: Response<ProductDetail?>) {
                if (response.isSuccessful) {
                    val productDetail = response.body() as ProductDetail?
                    Glide.with(this@ProductDetailActivity).load(productDetail!!.Product!!.ImageURL).into(ivProduct!!)
                    tvProduct!!.text = productDetail.Product!!.ItemDescription
                    tvPrice!!.text = "$" + productDetail.Product!!.Price!!.Price
                    tvBarcode!!.text = productDetail.Product!!.Barcode
                    if (productDetail.Product!!.Price!!.Type == "CLR") {
                        ivClearance!!.visibility = View.VISIBLE
                    } else {
                        ivClearance!!.visibility = View.GONE
                    }
                } else {
                    Toast.makeText(this@ProductDetailActivity, "Get product detail failed!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<ProductDetail?>, t: Throwable) {
                Toast.makeText(this@ProductDetailActivity, "Get product detail failed!", Toast.LENGTH_SHORT).show()
            }
        })
        */
    }

    companion object {
        const val FLAG_INTENT_SOURCE_SCAN = 10001
        const val FLAG_INTENT_SOURCE_SEARCH = 10002
        const val FLAG_BAR_CODE = "barCode"
        const val FLAG_INTENT_SOURCE = "intentSource"
    }
}
