package nz.co.warehouseandroidtest.ui.main

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.os.Bundle
import android.content.Intent
import nz.co.warehouseandroidtest.utils.PreferenceUtil
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.lifecycleScope
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import nz.co.warehouseandroidtest.*
import nz.co.warehouseandroidtest.base.BaseActivity
import nz.co.warehouseandroidtest.data.*
import nz.co.warehouseandroidtest.databinding.ActivityMainBinding
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.ui.scan.BarScanActivity
import nz.co.warehouseandroidtest.ui.search.SearchActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private val viewModel: MainViewModel by viewModels<MainViewModel>()

    private lateinit var binding: ActivityMainBinding
    private var permissions = arrayOf(Manifest.permission.CAMERA)
    private var checker = PermissionChecker(this)

    private var tvScan: TextView? = null
    private var tvSearch: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)
            tvScanBarcode.setOnClickListener {
                val intent = Intent()
                intent.setClass(this@MainActivity, BarScanActivity::class.java)
                startActivity(intent)
            }
            tvSearch.setOnClickListener {
                val intent = Intent()
                intent.setClass(this@MainActivity, SearchActivity::class.java)
                startActivity(intent)
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        if (checker.ifLackPermissions(permissions)) {
            PermissionActivity.startActivityForResult(this, REQUEST_PERMISSION_CODE, permissions)
        }
        lifecycleScope.launch {
            viewModel.loadUser().distinctUntilChanged().observe(this@MainActivity){
                println("user: $it")
                // if (it is DomainResult.Error) throw it.throwable
            }
        }
        if (PreferenceUtil.getUserId(this) == null) {
            /*
            checkNotNull((applicationContext as? WarehouseTestApp)).warehouseService.newUserId.enqueue(object : Callback<User?> {
                override fun onResponse(call: Call<User?>, response: Response<User?>) {
                    if (response.isSuccessful) {
                        val user = response.body()
                        if (user?.UserID != null) {
                            PreferenceUtil.putUserId(this@MainActivity, user.UserID)
                        }
                    } else {
                        Toast.makeText(this@MainActivity, "Get User failed!", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<User?>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "Get User failed!", Toast.LENGTH_SHORT).show()
                }
            })*/

        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (RESULT_OK != resultCode) {
            return
        }
        if (requestCode == REQUEST_PERMISSION_CODE && resultCode == PermissionActivity.PERMISSION_DENIED) {
            finish()
        }
    }

    companion object{
        private const val REQUEST_PERMISSION_CODE = 0
    }
}
