package nz.co.warehouseandroidtest.ui.search

abstract class EndlessRecyclerOnScrollListener : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
    private var isSlidingUpward = false
    override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        val manager = recyclerView.layoutManager as androidx.recyclerview.widget.LinearLayoutManager
        if (newState == androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE) {
            val lastItemPosition = manager.findLastCompletelyVisibleItemPosition()
            val itemCount = manager.itemCount
            if (lastItemPosition == itemCount - 1 && isSlidingUpward) {
                onLoadMore()
            }
        }
    }

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        isSlidingUpward = dy > 0
    }

    abstract fun onLoadMore()
}
