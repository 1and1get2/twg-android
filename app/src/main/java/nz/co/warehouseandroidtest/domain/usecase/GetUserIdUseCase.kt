package nz.co.warehouseandroidtest.domain.usecase

import nz.co.warehouseandroidtest.domain.source.WarehouseRepo
import javax.inject.Inject


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

class GetUserIdUseCase @Inject constructor (private val repo: WarehouseRepo) {
    suspend fun getUserId(forceUpdate: Boolean = false) = repo.getUserId(forceUpdate)
}
