package nz.co.warehouseandroidtest.domain.source.datastore

import nz.co.warehouseandroidtest.api.ApiResponse
import nz.co.warehouseandroidtest.api.WarehouseService
import nz.co.warehouseandroidtest.data.ProductDetail
import nz.co.warehouseandroidtest.data.SearchResult
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.domain.toDomainResult
import retrofit2.Response
import javax.inject.Inject


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

class WarehouseRemoteDataSource @Inject constructor(
    private val api: WarehouseService
    ) {
    suspend fun getNewUser() = ApiResponse.create(api.getNewUserId())

    suspend fun getProductDetail(paramMap: Map<String?, String?>?) = api.getProductDetail(paramMap)

    suspend fun getSearchResult(paramMap: Map<String?, String?>?) = api.getSearchResult(paramMap)
}
