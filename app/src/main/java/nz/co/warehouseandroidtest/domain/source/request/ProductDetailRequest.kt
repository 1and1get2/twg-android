package nz.co.warehouseandroidtest.domain.source.request

import com.google.gson.annotations.SerializedName


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

data class ProductDetailRequest(
    @SerializedName("Barcode")
    val barcode: String?,
    @SerializedName("MachineID")
    val machineID: String?,
    @SerializedName("UserID")
    val userID: String?,
    @SerializedName("Branch")
    val branch: String?,
    @SerializedName("DontSave")
    val dontSave: String?

)
