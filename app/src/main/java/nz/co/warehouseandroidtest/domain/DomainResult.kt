package nz.co.warehouseandroidtest.domain


import kotlinx.coroutines.CancellationException
import nz.co.warehouseandroidtest.api.*
import retrofit2.Response
import java.lang.Exception


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */


/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */

internal class GenericNetworkException(
    val apiError: ApiErrorResponse<*>,
    msg: String? = apiError.errorMessage
) : Exception(msg)

@Suppress("MemberVisibilityCanBePrivate")
sealed class DomainResult<out T> {

    sealed class Success<out T> : DomainResult<T>(){
        class SuccessResult<T>(val data: T) : Success<T>()
        object SuccessEmpty : Success<Nothing>()

        override fun toString(): String = when(this) {
            is SuccessResult -> "SuccessResult[data:$data]"
            is SuccessEmpty -> "SuccessEmpty"
        }
    }

    sealed class Loading<out T> : DomainResult<T>() {
        class LoadingWithData<T>(val data: T?) : Loading<T>()
        object LoadingEmpty : Loading<Nothing>()

        override fun toString(): String = when(this) {
            is LoadingEmpty -> "LoadingEmpty"
            is LoadingWithData -> "LoadingWithData[data:$data]"
        }
    }

    open class Error<out T>(
        val throwable: Throwable,
        val data: T? = null,
        val message: String? = throwable.message
    ) : DomainResult<T>() {

        open class ApiError<T>(
            val apiErrorResponse: ApiErrorResponse<T>,
            data: T? = null,
        ) : Error<T>(GenericNetworkException(apiErrorResponse), data, apiErrorResponse.errorMessage) {
            override fun toString(): String = "ApiError:$apiErrorResponse, data:$data"
        }

        override fun toString(): String = "throwable:${throwable.message}, data:$data, message:$message"
    }

    override fun toString(): String = when(this) {
        is Loading<*> -> "Loading[${super.toString()}]"
        is Success -> "Success[${super.toString()}]"
        is Error -> "Error[${super.toString()}]"
    }

    companion object {

        val loadingEmpty get() = Loading.LoadingEmpty
        fun <T> loading(data: T?) = Loading.LoadingWithData(data)

        val successEmpty get() = Success.SuccessEmpty
        fun <T> success(data: T) = Success.SuccessResult(data)

        fun <T> error(throwable: Throwable, data: T? = null, message: String? = throwable.message) = Error(throwable, data, message)
        fun <T> error(apiResponseError: ApiErrorResponse<T>, data: T? = null) : DomainResult<T> = Error.ApiError<T>(apiResponseError, data)

    }
}

/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val DomainResult<*>.succeeded get() = this is DomainResult.Success

fun <T, R>DomainResult<T>.map(mapper: T.() -> R) : DomainResult<R> {

    return try {
        when(this){
            is DomainResult.Success.SuccessResult<T> -> {
                DomainResult.Success.SuccessResult(mapper.invoke(data))
            }
            is DomainResult.Success.SuccessEmpty -> DomainResult.Success.SuccessEmpty

            is DomainResult.Error -> DomainResult.Error<R>(
                this.throwable,
                data?.let{mapper.invoke(it)},
                message
            )
            is DomainResult.Loading.LoadingEmpty -> this
            is DomainResult.Loading.LoadingWithData -> DomainResult.Loading.LoadingWithData(
                data?.let{mapper.invoke(it)}
            )

        }
    } catch (e: CancellationException) {
        throw e
    } catch (e: Exception) {
        DomainResult.Error(e, null)
    }
}
