package nz.co.warehouseandroidtest.domain.source.datastore


import androidx.lifecycle.LiveData
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.domain.source.localstore.WarehouseUserLocalDataStore
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvider
import javax.inject.Inject


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

class WarehouseLocalDataSource @Inject constructor(
    private val dispatchers: CoroutineDispatcherProvider,
    private val localDataStore: WarehouseUserLocalDataStore
) {
    val userLiveData : LiveData<User?> = localDataStore.userLiveData

    suspend fun putUser(user: User) = localDataStore.putUser(user)

}
