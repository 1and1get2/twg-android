package nz.co.warehouseandroidtest.domain.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.domain.map
import nz.co.warehouseandroidtest.domain.source.datastore.WarehouseLocalDataSource
import nz.co.warehouseandroidtest.domain.source.datastore.WarehouseRemoteDataSource
import nz.co.warehouseandroidtest.repository.networkBoundResource
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvider
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvisioned
import java.lang.RuntimeException
import javax.inject.Inject


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

class WarehouseRepo @Inject constructor(
    private val remoteDataSource: WarehouseRemoteDataSource,
    private val localDataSource: WarehouseLocalDataSource,
    private val coroutineDispatcherProvider: CoroutineDispatcherProvider
) : CoroutineDispatcherProvisioned by coroutineDispatcherProvider {

    suspend fun getUserId(forceUpdate: Boolean = false) : LiveData<DomainResult<String>> = getUser(forceUpdate).map {
        it.map {
            this?.UserID ?: throw RuntimeException("unable to get user:$it")
        }
    }

    private suspend fun getUser(forceUpdate: Boolean = false) : LiveData<DomainResult<User?>> = networkBoundResource(
        saveCallResult = localDataSource::putUser,
        shouldFetch = { forceUpdate },
        fetch = { remoteDataSource.getNewUser() },
        loadFromDb = { localDataSource.userLiveData }
    )
}
