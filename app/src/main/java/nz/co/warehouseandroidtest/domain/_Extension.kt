package nz.co.warehouseandroidtest.domain

import nz.co.warehouseandroidtest.api.ApiEmptyResponse
import nz.co.warehouseandroidtest.api.ApiErrorResponse
import nz.co.warehouseandroidtest.api.ApiResponse
import nz.co.warehouseandroidtest.api.ApiSuccessResponse
import retrofit2.Response
import java.lang.Exception
import java.lang.RuntimeException


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

private const val UNKNOWN_ERROR_STR = "unknown error"

val <T> Response<T>.errorMessage: String get() {
    val errorBodyString = errorBody()?.string()
    return errorBodyString.let {
        if (it.isNullOrEmpty()) message() else it
    } ?: UNKNOWN_ERROR_STR
}


inline fun <T> Response<T>.toDomainResult() : DomainResult<T> {
    val result = body()
    return when{
        isSuccessful && result != null -> DomainResult.success(result)
        isSuccessful -> DomainResult.successEmpty
        else -> {
            val code = code()
            val message = errorMessage
            DomainResult.error(ApiErrorResponse(message, code))
        }
    }
}



inline fun <T> ApiResponse<T>.toDomainResult() : DomainResult<T> {
    return when(this) {
        is ApiEmptyResponse -> DomainResult.successEmpty
        is ApiSuccessResponse<T> -> DomainResult.success(body)
        is ApiErrorResponse<T> -> DomainResult.error(this)
    }
}

