package nz.co.warehouseandroidtest.domain.source.localstore

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import dagger.hilt.android.qualifiers.ApplicationContext
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvider
import javax.inject.Inject


/**
 * Created by  on 3/5/21.
 * Project: twg-android-test
 * @see <a href="https://github.com/hereisderek/">Github</a>
 */

interface WarehouseUserLocalDataStore {
    val userLiveData : LiveData<User?>
    suspend fun putUser(user: User)
}

class WarehouseUserLocalDataStoreImpl @Inject constructor(
    @ApplicationContext context: Context,
    private val dispatchers: CoroutineDispatcherProvider
) : WarehouseUserLocalDataStore {
    private val sharedPref by lazy { context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE) }

    private val userId = sharedPref.stringLiveData(PREF_USER_ID, null)
    private val prodQAT = sharedPref.stringLiveData(PREF_USER_ID, null)

    private val _userLiveData = MediatorLiveData<User?>().apply {
        addSource(userId) {
            postValue(emitUser())
        }
        addSource(prodQAT) {
            postValue(emitUser())
        }
    }
    override val userLiveData: LiveData<User?> get() = _userLiveData


    private fun emitUser(ProdQAT: String? = prodQAT.value, UserID: String? = userId.value) : User? {
        if (ProdQAT.isNullOrEmpty() || UserID.isNullOrEmpty()) return null
        return User(ProdQAT, UserID)
    }

    override suspend fun putUser(user: User) {
        println("putUser:$user")
        sharedPref.edit()
            .putString(PREF_USER_ID, user.UserID)
            .putString(PREF_PROD_QAT, user.ProdQAT)
            .apply()
    }
    /*override suspend fun putUser(user: User) = dispatchers.onIO {

    }*/

    companion object {
        private const val PREF_USER_ID = "user_id"
        private const val PREF_PROD_QAT = "pref_prod_qat"
        private const val PREF_NAME = "warehouse-pref-name"
    }
}
