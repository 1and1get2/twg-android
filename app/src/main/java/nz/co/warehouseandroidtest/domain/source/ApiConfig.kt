package nz.co.warehouseandroidtest.domain.source


/**
 * Created by Derek Zhu on 1/5/21.
 * Project: twg-android-test
 *
 */

data class ApiConfig(
    val hostUrl: String,
    val subscriptionKey: String
)
