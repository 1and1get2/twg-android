package nz.co.warehouseandroidtest.domain.source.datastore

import nz.co.warehouseandroidtest.data.ProductDetail
import nz.co.warehouseandroidtest.data.SearchResult
import nz.co.warehouseandroidtest.data.User
import nz.co.warehouseandroidtest.domain.DomainResult
import retrofit2.Response
import retrofit2.http.QueryMap


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

interface WarehouseDataSource {
    suspend fun getNewUserId(): DomainResult<User>
    suspend fun getProductDetail(@QueryMap paramMap: Map<String?, String?>?): DomainResult<ProductDetail>
    suspend fun getSearchResult(@QueryMap paramMap: Map<String?, String?>?): DomainResult<SearchResult>
}
