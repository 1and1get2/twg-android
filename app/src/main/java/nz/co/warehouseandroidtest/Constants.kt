package nz.co.warehouseandroidtest

object Constants {
    const val HTTP_URL_ENDPOINT = "https://twg.azure-api.net/"
    const val PREF_USER_ID = "userId"
    const val BRANCH_ID = 208
    const val SUBSCRIPTION_KEY = "e2c60331e357474e8495a60e28dd42b6" // Please fill in the SUBSCRIPTION_KEY given to you here.
    const val MACHINE_ID = "1234567890"
}
