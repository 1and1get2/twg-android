package nz.co.warehouseandroidtest.data

class SearchResultItem {
    var Description: String? = null
    var Products: List<ProductWithoutPrice?>? = null
}
