package nz.co.warehouseandroidtest.data

data class Price(
    var Price: String? = null,
    var Type: String? = null
)
