package nz.co.warehouseandroidtest.utils.calladapter

import androidx.lifecycle.LiveData
import nz.co.warehouseandroidtest.api.ApiResponse
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.domain.toDomainResult
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Created by  on 3/5/21.
 * Project: twg-android-test
 *
 */

abstract class SimpleLiveDataCallAdapter<R, T>(
    protected val responseType: Type
) : CallAdapter<R, LiveData<T>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<R>): LiveData<T> = object : LiveData<T>() {
        private var started = AtomicBoolean(false)
        override fun onActive() {
            super.onActive()
            if (started.compareAndSet(false, true)) {
                call.enqueue(object: Callback<R>{
                    override fun onResponse(call: Call<R>, response: Response<R>) {
                        postValue(
                            this@SimpleLiveDataCallAdapter.onResponse(call, response)
                        )
                    }

                    override fun onFailure(call: Call<R>, t: Throwable) {
                        postValue(
                            this@SimpleLiveDataCallAdapter.onFailure(call, t)
                        )
                    }
                })
            }
        }
    }

    abstract fun onResponse(call: Call<R>, response: Response<R>) : T
    abstract fun onFailure(call: Call<R>, t: Throwable) : T
}


class LiveDataApiResponseCallAdapter<R>(
    responseType: Type
) : SimpleLiveDataCallAdapter<R, ApiResponse<R>>(responseType){
    override fun onResponse(call: Call<R>, response: Response<R>): ApiResponse<R> = ApiResponse.create(response)

    override fun onFailure(call: Call<R>, t: Throwable): ApiResponse<R> = ApiResponse.create(t)
}



class LiveDataDomainResultCallAdapter<R>(
    responseType: Type
) : SimpleLiveDataCallAdapter<R, DomainResult<R>>(responseType){
    override fun onResponse(call: Call<R>, response: Response<R>): DomainResult<R> = response.toDomainResult()

    override fun onFailure(call: Call<R>, t: Throwable): DomainResult<R> = DomainResult.error(t)
}


