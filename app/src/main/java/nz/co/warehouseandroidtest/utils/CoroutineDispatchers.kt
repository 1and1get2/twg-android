package nz.co.warehouseandroidtest.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */


interface CoroutineDispatcherProvisioned {
    val main: CoroutineDispatcher
    val computation: CoroutineDispatcher
    val io: CoroutineDispatcher
}

suspend fun <T> CoroutineDispatcher.onDispatcher(block: (() -> T)) = withContext(this){ block.invoke() }

suspend fun <T> CoroutineDispatcherProvisioned.onMain(block: (() -> T)) = main.onDispatcher(block)
suspend fun <T> CoroutineDispatcherProvisioned.onComputation(block: (() -> T)) = computation.onDispatcher(block)
suspend fun <T> CoroutineDispatcherProvisioned.onIO(block: (() -> T)) = io.onDispatcher(block)


@Suppress("MemberVisibilityCanBePrivate")
data class CoroutineDispatcherProvider(
    override val main: CoroutineDispatcher,
    override val computation: CoroutineDispatcher,
    override val io: CoroutineDispatcher
) : CoroutineDispatcherProvisioned {

    // suspend fun <T> onMain(block: (() -> T)) = main.onDispatcher(block)
    // suspend fun <T> onComputation(block: (() -> T)) = computation.onDispatcher(block)
    // suspend fun <T> onIO(block: (() -> T)) = io.onDispatcher(block)
}
