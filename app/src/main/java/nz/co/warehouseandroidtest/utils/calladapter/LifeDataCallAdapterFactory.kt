package nz.co.warehouseandroidtest.utils.calladapter

import androidx.lifecycle.LiveData
import nz.co.warehouseandroidtest.api.ApiResponse
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


/**
 * Created by  on 3/5/21.
 * Project: twg-android-test
 * @see <a href="https://github.com/hereisderek/">Github</a>
 */

class LifeDataCallAdapterFactory : CallAdapter.Factory() {

    override fun get(returnType: Type, annotations: Array<out Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        if (getRawType(returnType) != LiveData::class) {
            return null
        }

        val observableType = getParameterUpperBound(0, returnType as ParameterizedType)
        val rawObservableType = getRawType(observableType)
        require(rawObservableType == ApiResponse::class.java) { "type must be a resource" }
        require(observableType is ParameterizedType) { "resource must be parameterized" }
        val bodyType = getParameterUpperBound(0, observableType as ParameterizedType)
        return LiveDataApiResponseCallAdapter<Any>(bodyType)

    }
}
