package nz.co.warehouseandroidtest.repository

import androidx.annotation.MainThread
import androidx.lifecycle.*
import kotlinx.coroutines.*
import nz.co.warehouseandroidtest.api.ApiEmptyResponse
import nz.co.warehouseandroidtest.api.ApiErrorResponse
import nz.co.warehouseandroidtest.api.ApiResponse
import nz.co.warehouseandroidtest.api.ApiSuccessResponse
import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvider
import retrofit2.Response
import kotlin.coroutines.suspendCoroutine


/**
 * Created by  on 3/5/21.
 * Project: twg-android-test
 *
 */



fun <ResultType, RequestType> networkBoundResource(
    saveCallResult: suspend (RequestType) -> Unit,
    shouldFetch: (ResultType) -> Boolean = { true },
    loadFromDb: () -> LiveData<ResultType>,
    fetch: suspend () -> ApiResponse<RequestType>,
    processResponse: (suspend (ApiSuccessResponse<RequestType>) -> RequestType) = { it.body },
    onFetchFailed: ((ApiErrorResponse<RequestType>) -> Unit)? = null
): LiveData<DomainResult<ResultType>> {
    return CoroutineNetworkBoundResource(
        saveCallResult = saveCallResult,
        shouldFetch = shouldFetch,
        loadFromDb = loadFromDb,
        fetch = fetch,
        processResponse = processResponse,
        onFetchFailed = onFetchFailed
    ).asLiveData().distinctUntilChanged()
}


/**
 * A [NetworkBoundResource] implementation in corotuines
 */
private class CoroutineNetworkBoundResource<ResultType, RequestType>
@MainThread constructor(
    private val saveCallResult: suspend (RequestType) -> Unit,
    private val shouldFetch: (ResultType) -> Boolean = { true },
    private val loadFromDb: () -> LiveData<ResultType>,
    private val fetch: suspend () -> ApiResponse<RequestType>,
    private val processResponse: (suspend (ApiSuccessResponse<RequestType>) -> RequestType),
    private val onFetchFailed: ((ApiErrorResponse<RequestType>) -> Unit)?
) {

    private val result = liveData<DomainResult<ResultType>> {


        val dbSource = loadFromDb()
        val initialValue = dbSource.await()
        val willFetch = initialValue == null || shouldFetch(initialValue)
        if (!willFetch) {
            // if we won't fetch, just emit existing db values as success
            emitSource(
                dbSource.map { DomainResult.success(it) }
            )
        } else {
            doFetch(dbSource, this)
        }
    }

    private suspend fun doFetch(
        dbSource: LiveData<ResultType>,
        liveDataScope: LiveDataScope<DomainResult<ResultType>>
    ) {
        // emit existing values as loading while we fetch
        val initialSource = liveDataScope.emitSource(dbSource.map {
            DomainResult.loading(it)
        })
        val response = fetchCatching()
        when (response) {
            is ApiSuccessResponse, is ApiEmptyResponse -> {
                if (response is ApiSuccessResponse) {
                    val processed = processResponse(response)
                    initialSource.dispose()
                    // before saving it, disconnect it so that new values comes w/ success
                    saveCallResult(processed)
                }
                liveDataScope.emitSource(loadFromDb().map {
                    DomainResult.success(it)
                })
            }
            is ApiErrorResponse -> {
                onFetchFailed?.invoke(response)
                liveDataScope.emitSource(
                    dbSource.map {
                        val error = ApiErrorResponse<ResultType>(response.errorMessage, response.code)
                        DomainResult.error<ResultType>(apiResponseError = error, data = it)
                    }
                )
            }
        }
    }

    // temporary here during migration
    fun asLiveData() = result as LiveData<DomainResult<ResultType>>

    private suspend fun fetchCatching(): ApiResponse<RequestType> {
        return try {
            fetch()
        } catch (ex: CancellationException) {
            throw ex
        } catch (ex: Throwable) {
            ApiResponse.create(ex)
        }
    }

    private suspend fun <T> LiveData<T>.await() = withContext(Dispatchers.Main) {
        val receivedValue = CompletableDeferred<T?>()
        val observer = Observer<T> {
            if (receivedValue.isActive){
                receivedValue.complete(it)
            }
        }
        try {
            observeForever(observer)
            return@withContext receivedValue.await()
        } finally {
            removeObserver(observer)
        }
    }
}
