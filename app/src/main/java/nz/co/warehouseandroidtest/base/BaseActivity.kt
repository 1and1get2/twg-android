package nz.co.warehouseandroidtest.base

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */

@AndroidEntryPoint
abstract class BaseActivity : AppCompatActivity() {

}
