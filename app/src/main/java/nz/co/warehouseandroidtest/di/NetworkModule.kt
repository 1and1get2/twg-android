package nz.co.warehouseandroidtest.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import nz.co.warehouseandroidtest.Constants
import nz.co.warehouseandroidtest.domain.source.ApiConfig
import nz.co.warehouseandroidtest.api.WarehouseService
import nz.co.warehouseandroidtest.utils.calladapter.LifeDataCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Derek Zhu on 1/5/21.
 * Project: twg-android-test
 *
 */

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideApiConfig(): ApiConfig = ApiConfig(
        Constants.HTTP_URL_ENDPOINT, Constants.SUBSCRIPTION_KEY
    )

    @Provides
    fun provideAuthInterceptor(apiConfig: ApiConfig): Interceptor = Interceptor{ chain ->
        chain.run {
            val request = request()
                .newBuilder()
                .addHeader("Ocp-Apim-Subscription-Key", apiConfig.subscriptionKey)
                .build()
            proceed(request)
        }
    }

    @Provides
    fun provideOkHttp(interceptor: Interceptor) : OkHttpClient = OkHttpClient.Builder().run{
        addInterceptor(interceptor)
        build()
    }

    @Provides
    fun provideRetrofit(
        apiConfig: ApiConfig,
        okHttpClient: OkHttpClient
    ) : Retrofit = Retrofit.Builder().baseUrl(apiConfig.hostUrl)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .addCallAdapterFactory(LifeDataCallAdapterFactory())
        .client(okHttpClient)
        .build()

    @Provides
    fun provideWarehouseService(retrofit: Retrofit) : WarehouseService = retrofit.create(WarehouseService::class.java)

}
