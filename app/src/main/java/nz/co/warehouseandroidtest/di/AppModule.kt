package nz.co.warehouseandroidtest.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import nz.co.warehouseandroidtest.domain.source.localstore.WarehouseUserLocalDataStore
import nz.co.warehouseandroidtest.domain.source.localstore.WarehouseUserLocalDataStoreImpl
import nz.co.warehouseandroidtest.utils.CoroutineDispatcherProvider


/**
 * Created by Derek Zhu on 2/5/21.
 * Project: twg-android-test
 *
 */


@Module (includes = [NetworkModule::class, AppModuleBinding::class])
@InstallIn(SingletonComponent::class)
object AppModule {



    @Provides
    fun provideAppCoroutineDispatcherProvider() : CoroutineDispatcherProvider = CoroutineDispatcherProvider(
        Dispatchers.Main,
        Dispatchers.Default,
        Dispatchers.IO
    )
}

@Module
@InstallIn(SingletonComponent::class)
interface AppModuleBinding {
    @Binds
    fun bindWarehouseUserLocalDataStore(dataStore: WarehouseUserLocalDataStoreImpl) : WarehouseUserLocalDataStore


}
