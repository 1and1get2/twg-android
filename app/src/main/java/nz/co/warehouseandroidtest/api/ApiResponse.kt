package nz.co.warehouseandroidtest.api

import nz.co.warehouseandroidtest.domain.DomainResult
import nz.co.warehouseandroidtest.domain.errorMessage
import retrofit2.Response
import java.lang.Exception
import java.util.regex.Pattern


/**
 * Created by  on 2/5/21.
 * Project: twg-android-test
 *
 */


/**
 * Common class used by API responses.
 * @param <T> the type of the response object
</T> */



@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<out T>(
    val code: Int
) {

    val isSuccessful : Boolean = this !is ApiErrorResponse

    override fun toString(): String = "${this.javaClass.simpleName}[code:$code]"

    companion object {
        private const val UNKNOWN_ERROR_STR = "unknown error"
        private const val UNKNOWN_ERROR_CODE = -1

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(error.message ?: UNKNOWN_ERROR_STR, UNKNOWN_ERROR_CODE)
        }


        fun <T> create(response: Response<T>) : ApiResponse<T> = response.run {
            val code = code()
            val body = body()

            when {
                isSuccessful && (body == null || code == 204) -> ApiEmptyResponse(code)
                isSuccessful && body != null -> ApiSuccessResponse(body, code)
                else -> ApiErrorResponse(errorMessage, code)
            }
        }
    }

}


/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse(code: Int) : ApiResponse<Nothing>(code)

class ApiSuccessResponse<T>(val body: T, code: Int) : ApiResponse<T>(code){
    override fun toString(): String = "ApiSuccessResponse[code:$code]"
}

class ApiErrorResponse<T>(val errorMessage: String, code: Int) : ApiResponse<T>(code) {
    override fun toString(): String = "ApiErrorResponse[code:$code, errorMessage:$errorMessage]"
}



