package nz.co.warehouseandroidtest.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import nz.co.warehouseandroidtest.data.*
import retrofit2.Response
import retrofit2.http.QueryMap

interface WarehouseService {
    @GET("bolt/newuser.json")
    suspend fun getNewUserId(): Response<User>

    @GET("bolt/price.json")
    suspend fun getProductDetail(@QueryMap paramMap: Map<String?, String?>?): Response<ProductDetail>

    @GET("bolt/search.json")
    suspend fun getSearchResult(@QueryMap paramMap: Map<String?, String?>?): Response<SearchResult>
}
